import requests

URL_TEMPLATES_LOCAL = 'http://localhost:3000/api/v1/templates/migrate'
URL_TEMPLATES_STAGE = 'http://u3dev.lavenirapps.co:3001/api/v1/templates/migrate'


def upload_template(template):
    response = requests.post(URL_TEMPLATES_LOCAL, json=template)
    print("Uploading")
    print(response)
    if response.status_code != 201:
        if response.status_code != 201:
            print(f"Error en la plantilla {template['id']} - Error {response.content}")
        else:
            print(f'Response {response.status_code} - template {template["id"]}')
