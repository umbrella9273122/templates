import json
import uuid
from multiprocessing import Pool

from db_repository import fetch_all, fetch_one


def get_ids_templates(template):
    template_id = int(template[0])
    return str(template_id)


def convert_to_map(records):
    dic_typings = {}
    for i in records:
        dic_typings = {
            **dic_typings,
            f'{int(i[0])}': {
                'key': int(i[0]),
                'value': i[1]
            }
        }
    return dic_typings


def get_all_typings():
    sql_typings = "SELECT id, name FROM sys_typing"
    records = fetch_all(sql_typings)
    return convert_to_map(records)


def get_all_roles():
    sql_roles = "SELECT id, name FROM sys_role"
    records = fetch_all(sql_roles)
    return convert_to_map(records)


def get_approvements(ids_templates):
    sql_query = f'SELECT id, created_by, role_id, user_id, sort, prev_status, next_status, temp_id '
    sql_query += f'FROM sv_task_temp_approb WHERE temp_id in ({ids_templates})'
    return fetch_all(sql_query)


def proccess_aprovements(i):
    id_aprovement = i[0]
    created_by = i[1]
    role_id = i[2]
    user_id = i[3]
    sort = i[4]
    prev_status = i[5]
    next_status = i[6]
    temp_id = i[7]

    return {
        "code": int(id_aprovement),
        "created_by": int(created_by),
        "role": "undefined",
        "role_id": role_id,
        "user_id": user_id,
        "user": "no user name",
        "prev_status": prev_status,
        "next_status": next_status,
        "orden": sort,
        "id": str(uuid.uuid4()).split('-')[0],
        "key": str(uuid.uuid4()).split('-')[0],
        "allowEditions": False,
        "temp_id": int(temp_id)
    }


def map_approvements(typings, roles, output_approvements):
    """
    :param typings:
    :param roles:
    :param output_approvements:
    :return:
    """

    def convert_and_validate_status(status, name_status, current_object):
        """
        :param status:
        :param name_status:
        :param current_object:
        :return:
        """
        if status == '1' or status == '2' or status == '3' or status == '4':
            current_object = {
                **current_object,
                f"{name_status}": f"Status {status} - U2 (1,2,3,4)",
            }
        elif status in typings:
            current_object = {
                **current_object,
                f"{name_status}": typings[status]['value'],
            }

        return current_object

    def get_name_user(approvement):
        """
        :param approvement:
        :return:
        """
        name_user = ""
        if approvement['user_id'] is not None:
            if len(approvement['user_id']) > 0:
                users = []
                for i in approvement['user_id'].split(','):
                    if i != '':
                        users.append(int(i))
                if len(users) > 0:
                    name_user = fetch_one("SELECT id, name FROM bs_account WHERE id = %s", users[0])

        return name_user

    def get_name_roles(approvement):
        """
        Get the name associated to role
        :param approvement: it receives an approvement to set the name of roles
        :return: a format string with , that represents a list of roles
        """
        name_roles = ""
        if approvement['role_id'] is not None:
            if len(approvement['role_id']) > 0:
                current_roles = []
                for i in approvement['role_id'].split(','):
                    current_roles.append(roles[f'{i}']['value'])

                name_roles = ",".join(current_roles)

        return name_roles

    def apply_status_and_roles(approvement):
        current_approvement = {
            **approvement
        }
        prev_status = str(approvement['prev_status'])
        next_status = str(approvement['next_status'])

        current_approvement = {
            **current_approvement,
            **convert_and_validate_status(prev_status, 'statusold', current_approvement),
        }

        current_approvement = {
            **current_approvement,
            **convert_and_validate_status(next_status, 'nextStatus', current_approvement)
        }

        name_user = get_name_user(approvement)
        name_roles = get_name_roles(approvement)

        return {**current_approvement, "user": name_user, "role": name_roles}

    out_lambda = map(apply_status_and_roles, output_approvements)

    return list(out_lambda)


def retrieve_approvements(ids_templates):
    """

    :param ids_templates:
    :return:
    """
    typings = get_all_typings()
    roles = get_all_roles()
    approvements = get_approvements(ids_templates)

    # process to map
    pool_approvements = Pool(processes=30)
    output_approvements = pool_approvements.map(proccess_aprovements, approvements)

    json_approvals = map_approvements(typings, roles, output_approvements)

    approvals = {}
    for item in json_approvals:
        if item['temp_id'] in approvals:
            approvals[item['temp_id']].append(item)
        else:
            approvals = {
                **approvals,
                item['temp_id']: [item]
            }

    json_string = json.dumps(approvals, indent=4)
    json_file = open("output_approvements.json", "w")
    json_file.write(json_string)
    json_file.close()

    return approvals
