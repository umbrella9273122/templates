import asyncio
import datetime
import json
import time
import uuid
from json import dumps, loads
from multiprocessing import Pool

import numpy as np

from aprovements import retrieve_approvements
from db_repository import fetch_all
from profiles import get_profiles, get_profiles_async

# from aprovements import read_aprovements

templates = []


def get_ids_templates(template):
    template_id = int(template[0])
    return str(template_id)


def get_ids_templates_tuple(template):
    template_id = int(template[0])
    return (template_id,)


def template_tuple(template):
    return (template,)


def read_templates():
    tm1 = time.perf_counter()
    # LIMIT 10 : solo existen 496 registros
    sql_select_query = "SELECT id, site_id, created_by, name, description, custom, requeriments, "
    sql_select_query += "custom_status, access, accesedit, rule_reop, rule_reasign,  "
    sql_select_query += "no_status_role, third_list, no_status_list "
    sql_select_query += "FROM sv_task_temp "
    list_templates = fetch_all(sql_select_query)

    pool_database = Pool(processes=30)
    output_database = pool_database.map(get_ids_templates, list_templates)
    ids_templates = ",".join(output_database)

    templates_list_tuple = pool_database.map(template_tuple, list_templates)
    id_templates_tuple = pool_database.map(get_ids_templates_tuple, list_templates)

    approvals = retrieve_approvements(ids_templates)

    print("Process profiles")
    profiles = get_profiles(id_templates_tuple, templates_list_tuple)
    # profiles = asyncio.create_task(get_profiles_async(id_templates_tuple, templates_list_tuple))
    # result_task = await asyncio.gather(profiles)

    profiles_keys = ["accGroup", "cretGroup", "viedFroup", "userRpening", "userRgnment", "roleStatusRes",
                     "userRunService", "statusRestricted"]
    tuple_template_and_more = []
    for temp in list_templates:
        id_template = int(temp[0])
        profiles_for_current_temp = {}

        for profile_key in profiles_keys:
            profiles_for_current_temp = {
                **profiles_for_current_temp,
                profile_key: profiles[profile_key][id_template]
            }

        if id_template in approvals:
            tuple_template_and_more.append((temp, approvals[id_template], profiles_for_current_temp))
        else:
            tuple_template_and_more.append((temp, None, profiles_for_current_temp))

    templates_json = pool_database.map(proces_template, tuple_template_and_more, chunksize=50)

    json_string = json.dumps(templates_json, indent=4)
    json_file = open("templates_proccesed.json", "w")
    json_file.write(json_string)
    json_file.close()

    tm2 = time.perf_counter()
    print(f"Total time elapsed: {tm2 - tm1} seconds")
    pool_database.close()


def proces_template(row):
    # for row in records:
    template = row[0]
    approvals = row[1]
    profiles = row[2]

    template_id = template[0]
    site_id = template[1]
    created_by = template[2]
    name = template[3]
    description = template[4]
    custom = template[5]
    requeriments = template[6]
    custom_status = template[7]

    name_user = get_name_by_id(site_id, created_by)
    inputs = convert_inputs(custom)
    procedures = set_procedures_to_template(requeriments)
    status = set_custom_status(custom_status)

    sections = {
        "custom": {
            "id": str(uuid.uuid4()).split('-')[0],
            "inputs": inputs
        },
        "procedure": procedures,
        "aprovements": approvals,
        "profiles": profiles
    }

    template_out = {
        "id": f"11#template#{int(template_id)}",
        "code": str(uuid.uuid4()).split('-')[0],
        "enable": 1,
        "type": "template",
        "typeId": "11#template#task",
        "siteId": f'{int(site_id)}',
        "name": name or "Not assigment",
        "description": description or " ",
        "icon": "CheckCircleFilled",
        "createdById": f"{int(created_by)}",
        "createdByName": name_user or "Unsigned",
        "createdAt": get_created_at(),
        "updatedAt": get_created_at(),
        "sections": sections,
    }

    test_load_template = True

    if test_load_template:
        template_out = {
            **template_out,
            "statusAvailable": status,  # customStatus
            "profiles": {},
            "behavior": {},
            "app": {},
            # Created by the service automatically
            "searchFields": {
                "title": name.lower() if name_user is not None else " ",
                "createdByName": name_user.lower(),
                "createdById": f"{int(created_by)}",
                "description": description.lower() if description is not None else " "
            }
        }

    return template_out


def get_created_at():
    now = datetime.datetime.now()
    milliseconds = now.timestamp() * 1000
    return int(milliseconds)


def convert_inputs(custom):
    inputs = []
    if custom is not None:
        input_json = json.loads(dumps(loads(custom), separators=(',', ':')))
        array_inputs = np.array(input_json)

        for current_input in array_inputs:
            custom_input = {
                "id": str(uuid.uuid4()).split('-')[0],
                **current_input
            }
            input_type = custom_input["type"]
            if input_type == 'select' or input_type == 'radio-group' or input_type == 'checkbox-group':
                transform_data_values(custom_input)

            inputs.append(custom_input)

    return inputs


def transform_data_values(current_input):
    input_values = np.array(current_input['values'])
    values = []
    for value in input_values:
        current_value = {
            **value,
            "id": str(uuid.uuid4()).split('-')[0]
        }
        values.append(current_value)

    current_input['values'] = values


def set_procedures_to_template(process):
    procedures = []
    if process is not None:
        if len(process) != 0:
            custom_split = f"(" + process.replace("'", "") + ")"
            sql_select_procedures = f'SELECT id, name FROM sys_process WHERE id IN {custom_split}'
            rows = fetch_all(sql_select_procedures)

            for col in rows:
                id_process = col[0]
                name = col[1]
                procedures.append({"id": int(id_process), "name": name})

    return procedures


def set_custom_status(custom_status):
    status_dictionary = {}
    if len(custom_status) > 0:
        procedures = []
        for i in custom_status.split(","):
            if i != '':
                procedures.append(int(i))

        custom_split = f"(" + custom_status.replace("'", "") + ")"
        sql_select_procedures = f"SELECT id, name FROM sys_typing WHERE id IN {custom_split}"
        procedures_result = fetch_all(sql_select_procedures.replace(",)", ")"))
        for item in procedures_result:
            status_dictionary = {
                **status_dictionary,
                f"{item[1]}": {
                    "id": int(item[0]),
                    "name": item[1],
                    "selected": True
                }
            }
    return status_dictionary


def get_name_by_id(site_id, id_user):
    sql_statement = f"SELECT name FROM bs_account WHERE site_id = {site_id} AND user_id = {id_user}"
    records = fetch_all(sql_statement)
    if len(records) > 0:
        name = records.__getitem__(0)[0]
        return name
    else:
        return " "


if __name__ == '__main__':
    # asyncio.run(read_templates())
    read_templates()
