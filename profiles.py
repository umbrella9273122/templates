import asyncio
import json
from multiprocessing import Pool

from db_repository import fetch_all


#
# Profiles
#
async def get_profiles_async(ids_templates, templates_list_tuple):
    # 1.- seleccionar grupo de acceso para solo ver
    pool = Pool(processes=30)

    def convert_to_dict_profile(key, list_data_profile):
        profile = {}
        for item in list_data_profile:
            if item['temp_id'] in profile:
                profile[item['temp_id']].appendAll(item[key])
            else:
                profile = {**profile, item['temp_id']: [*item[key]]}
        return profile

    async def group_access_to_view():
        pool_only_view = pool.starmap_async(select_group_access_only_view, ids_templates, chunksize=20)
        list_only_view = pool_only_view.get()
        return list_only_view

    # 2.- seleccion del grupo creador
    async def group_creator():
        pool_group_creator = pool.starmap_async(select_group_creator, templates_list_tuple, chunksize=20)
        list_group_creator = pool_group_creator.get()
        return list_group_creator

    # 3.- selecciona  el grupo ver/editar
    async def group_view_edit():
        pool_group_view_edit = pool.starmap_async(select_group_view_edit, templates_list_tuple, chunksize=20)
        list_group_view_edit = pool_group_view_edit.get()
        return list_group_view_edit

    # 4.- usuarios /  reapertura de servicios
    async def group_reopening_services():
        pool_group_reopening_services = pool.starmap_async(select_group_reopening_services, templates_list_tuple,
                                                           chunksize=20)
        list_group_reopening_services = pool_group_reopening_services.get()
        return list_group_reopening_services

    # 5.- usuarios /  reasignacion de servicios
    async def group_reassing_services():
        pool_group_reassign_services = pool.starmap_async(select_group_reassign_services, templates_list_tuple,
                                                          chunksize=20)
        list_group_reassign_services = pool_group_reassign_services.get()
        return list_group_reassign_services

    # 6.- restriccion de estados al role
    async def group_restriction_status_role():
        pool_group_restriction_status_to_rol = pool.starmap_async(select_restriction_status_to_rol,
                                                                  templates_list_tuple,
                                                                  chunksize=20)
        list_restriction_status_to_rol = pool_group_restriction_status_to_rol.get()
        return list_restriction_status_to_rol

    # 7.- lista de terceros a ejecutar servicio (estricto)
    async def group_third_to_execute_service():
        pool_third_to_execute_service = pool.starmap_async(select_third_to_execute_service, templates_list_tuple,
                                                           chunksize=20)
        dict_third_to_execute_service = pool_third_to_execute_service.get()
        return dict_third_to_execute_service

    task_group_access_to_view = asyncio.create_task(group_access_to_view())
    task_group_craetor = asyncio.create_task(group_creator())
    task_group_view_edit = asyncio.create_task(group_view_edit())
    task_group_reopening_services = asyncio.create_task(group_reopening_services())
    task_group_reassing_services = asyncio.create_task(group_reassing_services())
    task_group_restriction_status_role = asyncio.create_task(group_restriction_status_role())
    task_group_third_to_execute_service = asyncio.create_task(group_third_to_execute_service())

    results = await asyncio.gather(
        task_group_access_to_view,
        task_group_craetor,
        task_group_view_edit,
        task_group_reopening_services,
        task_group_reassing_services,
        task_group_restriction_status_role,
        task_group_third_to_execute_service
    )

    return {
        "accGroup": convert_to_dict_profile('profiles', results[0]),
        "cretGroup": convert_to_dict_profile('roles', results[1]),
        "viedFroup": convert_to_dict_profile('roles', results[2]),
        "userRpening": convert_to_dict_profile('roles', results[3]),
        "userRgnment": convert_to_dict_profile('roles', results[4]),
        "roleStatusRes": convert_to_dict_profile('roles', results[5]),
        "userRunService": convert_to_dict_profile('users', results[6]),
    }


#
# Profiles
#
def get_profiles(ids_templates, templates_list_tuple):
    """

    :param ids_templates:
    :param templates_list_tuple:
    :return:
    """
    # pool_database = Pool(processes=30)
    # ids_templates = pool_database.map(get_ids_templates, list_templates)
    # templates_list_tuple = pool_database.map(template_tuple, list_templates)

    # 1.- seleccionar grupo de acceso para solo ver
    pool = Pool(processes=30)
    pool_only_view = pool.starmap_async(select_group_access_only_view, ids_templates, chunksize=20)
    list_only_view = pool_only_view.get()

    # 2.- seleccion del grupo creador
    pool_group_creator = pool.starmap_async(select_group_creator, templates_list_tuple, chunksize=20)
    list_group_creator = pool_group_creator.get()

    # 3.- selecciona  el grupo ver/editar
    pool_group_view_edit = pool.starmap_async(select_group_view_edit, templates_list_tuple, chunksize=20)
    list_group_view_edit = pool_group_view_edit.get()

    # 4.- usuarios /  reapertura de servicios
    pool_group_reopening_services = pool.starmap_async(select_group_reopening_services, templates_list_tuple,
                                                       chunksize=20)
    list_group_reopening_services = pool_group_reopening_services.get()

    # 5.- usuarios /  reasignacion de servicios
    pool_group_reassign_services = pool.starmap_async(select_group_reassign_services, templates_list_tuple,
                                                      chunksize=20)
    list_group_reassign_services = pool_group_reassign_services.get()

    # 6.- restriccion de estados al role
    pool_group_restriction_status_to_rol = pool.starmap_async(select_restriction_status_to_rol, templates_list_tuple,
                                                              chunksize=20)
    list_restriction_status_to_rol = pool_group_restriction_status_to_rol.get()

    # 7.- lista de terceros a ejecutar servicio (estricto)
    pool_third_to_execute_service = pool.starmap_async(select_third_to_execute_service, templates_list_tuple,
                                                       chunksize=20)
    dict_third_to_execute_service = pool_third_to_execute_service.get()
    print(dict_third_to_execute_service)

    # 8.- Lista restringida de estados

    poll_status_restrict = pool.starmap_async(select_status_restrict, templates_list_tuple, chunksize=20)
    dict_status_restrict = poll_status_restrict.get()

    def convert_to_dict_profile(key, list_data_profile):
        profile = {}
        for item in list_data_profile:
            if item['temp_id'] in profile:
                profile[item['temp_id']].appendAll(item[key])
            else:
                profile = {**profile, item['temp_id']: [*item[key]]}
        return profile

    profiles = {
        "accGroup": convert_to_dict_profile('profiles', list_only_view),
        "cretGroup": convert_to_dict_profile('roles', list_group_creator),
        "viedFroup": convert_to_dict_profile('roles', list_group_view_edit),
        "userRpening": convert_to_dict_profile('roles', list_group_reopening_services),
        "userRgnment": convert_to_dict_profile('roles', list_group_reassign_services),
        "roleStatusRes": convert_to_dict_profile('roles', list_restriction_status_to_rol),
        "userRunService": convert_to_dict_profile('users', dict_third_to_execute_service),
        "statusRestricted": convert_to_dict_profile('typings', dict_status_restrict),
    }

    pool.close()
    return profiles


# 1. seleccionar grupo de acceso para solo ver
# convert_group_access_only_view_to_dict(pool_only_view.get())
def convert_group_access_only_view_to_dict(gruop_access_only_view):
    profiles_dict = {}
    for item in gruop_access_only_view:
        if item['temp_id'] in profiles_dict:
            print("Existe")
            profiles_dict[item['temp_id']].append(item)
        else:
            profiles_dict = {
                **profiles_dict,
                item['temp_id']: item['profiles']
            }

    print(json.dumps(profiles_dict))


def select_group_access_only_view(template_id):
    sql_virtual_row = f"""
    SELECT value
    FROM sys_virtual_row
    WHERE module = 'sv_task_temp'
      AND value IS NOT NULL
      AND value <> ""
    AND value REGEXP '^[0-9].*$'
    AND parent_id = {template_id}
    ORDER BY parent_id
    """
    results_rows = fetch_all(sql_virtual_row)
    profiles = []
    for i in results_rows:
        list_items = list(i)
        ids_roles = ",".join(list_items)
        results = fetch_all(f"SELECT id, name, status FROM sys_role WHERE id in ({ids_roles})")
        if len(results) > 0:
            for item in results:
                profiles.append({
                    "value": int(item[0]),
                    "label": item[1]
                })
    return {"temp_id": template_id, "profiles": profiles}


def transform_roles_to_map(result_roles):
    roles = []
    for role in result_roles:
        roles.append({
            "value": int(role[0]),
            "label": role[1]
        })
    return roles


# Seleccionar el grupo creador
def select_group_creator(template):
    """
    
    :param template: 
    :return: 
    """""
    roles = []
    if template[9] is not None and template[9] != "":
        ids_roles = ",".join(template[9].split(','))
        sql_query_roles = f"SELECT id, name FROM sys_role WHERE id in ({ids_roles})"
        sql_query_roles = sql_query_roles.replace("(,", "(").replace(",)", ")")
        result_roles = fetch_all(sql_query_roles)
        if len(result_roles) > 0:
            roles = transform_roles_to_map(result_roles)
    return {"temp_id": int(template[0]), "roles": roles, "tag": "select_group_creator"}


# Seleccionar el grupo ver/editar
def select_group_view_edit(template):
    """
    Column: sv_task_temp.access
    :param template:
    :return:
    """
    roles = []
    if template[8] is not None and template[8] != "":
        ids_roles = ",".join(template[8].split(','))
        sql_query_roles = f"SELECT id, name FROM sys_role WHERE id in ({ids_roles});"
        sql_query_roles = sql_query_roles.replace("(,", "(").replace(",)", ")")
        result_roles = fetch_all(sql_query_roles)
        if len(result_roles) > 0:
            roles = transform_roles_to_map(result_roles)
    return {"temp_id": int(template[0]), "roles": roles, "tag": "select_group_view_edit"}


# Usuarios / reapertura de servicios
def select_group_reopening_services(template):
    """
    Column: sv_task_temp.rule_reop
    :param template:
    :return:
    """
    roles = []
    if template[10] is not None and template[10] != "":
        ids_roles = ",".join(template[10].split(','))
        sql_query_roles = f"SELECT id, name FROM sys_role WHERE id in ({ids_roles});"
        sql_query_roles = sql_query_roles.replace("(,", "(").replace(",)", ")")
        result_roles = fetch_all(sql_query_roles)
        if len(result_roles) > 0:
            roles = transform_roles_to_map(result_roles)
    return {"temp_id": int(template[0]), "roles": roles, "tag": "select_group_reopening_services"}


# Usuario / reasignación de servicios
def select_group_reassign_services(template):
    """
    Column: sv_task_temp.rule_reasign
    :param template:
    :return:
    """
    roles = []
    if template[11] is not None and template[11] != "":
        ids_roles = ",".join(template[11].split(','))
        sql_query_roles = f"SELECT id, name FROM sys_role WHERE id in ({ids_roles});"
        sql_query_roles = sql_query_roles.replace("(,", "(").replace(",)", ")")
        result_roles = fetch_all(sql_query_roles)
        if len(result_roles) > 0:
            roles = transform_roles_to_map(result_roles)
    return {"temp_id": int(template[0]), "roles": roles, "tag": "select_group_reassign_services"}


# Restriccion de Estados al Rol
def select_restriction_status_to_rol(template):
    """
    Column: sv_task_temp.no_status_role
    :param template:
    :return:
    """
    roles = []
    if template[12] is not None and template[12] != "":
        ids_roles = ",".join(template[12].split(','))
        sql_query_roles = f"SELECT id, name FROM sys_role WHERE id in ({ids_roles});"
        sql_query_roles = sql_query_roles.replace("(,", "(").replace(",)", ")")
        result_roles = fetch_all(sql_query_roles)
        if len(result_roles) > 0:
            roles = transform_roles_to_map(result_roles)
    return {"temp_id": int(template[0]), "roles": roles, "tag": "select_restriction_status_to_rol"}


# Lista de terceros a ejecutar servicio (estricto)
def select_third_to_execute_service(template):
    """
    Column: sv_task_temp.thir_list
    :param template:
    :return:
    """
    users = []
    if template[13] is not None and template[13] != "":
        ids_users = ",".join(template[13].split(','))
        sql_query_useres = f"SELECT id, name FROM bs_account WHERE id in ({ids_users});"
        sql_query_useres = sql_query_useres.replace("(,", "(").replace(",)", ")")
        result_users = fetch_all(sql_query_useres)
        if len(result_users) > 0:
            users = transform_roles_to_map(result_users)
    return {"temp_id": int(template[0]), "users": users, "tag": "select_third_to_execute_service"}


def select_status_restrict(template):
    """
    Column: sv_task_temp.no_status_list
    :param template:
    :return:
    """
    typings_list = []
    if template[14] is not None and template[14] != "":
        ids_typing = ",".join(template[14].split(','))
        sql_query_typings = f"SELECT id FROM sys_typing WHERE id in ({ids_typing});"
        sql_query_typings = sql_query_typings.replace("(,", "(").replace(",)", ")")
        result_typing = fetch_all(sql_query_typings)
        if len(result_typing) > 0:
            for typing in result_typing:
                for item in typing:
                    typings_list.append(int(item))
        print(f"{template[0]} {typings_list}")
        # if len(result_typing) > 0:

        #     typings = transform_roles_to_map(result_typing)
    return {"temp_id": int(template[0]), "typings": typings_list, "tag": "select_status_restrict"}
