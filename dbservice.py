import json
import boto3

# creating the DynamoDB client
dynamodb_client = boto3.client("dynamodb", region_name="us-east-1")
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table("dev_template")


def create_template(template):
    response = table.put_item(Item={
        **template
    })
    print(f"Template {template} {response['ResponseMetadata']['HTTPStatusCode']}")


def delete_templates():
    response = table.scan()
    for item in response['Items']:
        response = table.delete_item(
            Key={
                "id": item['id'],
            }
        )
        print(response)


def delete_item(item):
    response = table.delete_item(
        Key={
            "id": item
        }
    )
    print(response)


def read_index_templates():
    response = dynamodb_client.query(
        TableName="template",  # only used to read id's from templates
        IndexName="typeId-createdAt-index",
        KeyConditionExpression="typeId = :type_id",
        ExpressionAttributeNames={
            "#id": "id"
        },
        ExpressionAttributeValues={
            ":type_id": {"S": "11#template"}
        },
        ProjectionExpression="#id"
    )

    data = response['Items']

    print(response)

    # LastEvaluatedKey indicates that there are more results
    # while 'LastEvaluatedKey' in response:
    #     response = table.query(ExclusiveStartKey=response['LastEvaluatedKey'])
    #     data.update(response['Items'])
    # for item in response["Items"]:
    #    item = item['id']['S']
    #    print(item)
    # delete_item(item) # call method to delete templates


def get_all_templates(**kwargs):
    response = table.scan()
    data = response['Items']

    while 'LastEvaluatedKey' in response:
        response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
        data.extend(response['Items'])

    template_with_type_id = filter(template_contain_type_id, data)
    templates = filter(is_template, template_with_type_id)
    templates_enable = filter(template_is_enable, templates)

    counter = 0
    templates_to_update = []
    for i in templates_enable:
        templates_to_update.append({
            "id": i['id'],
            "typeId": i['typeId'],
            "searchFields": {
                "title": i['name'],
                "createdByName": i['createdByName'],
                "createdById": i['createdById'],
            }
        })
        print(f"counter {counter} - {i['typeId']} - {i['id']}")
        counter += 1

    print(f"Templates available {templates_enable.__sizeof__()}")
    json_string = json.dumps(templates_to_update, indent=4)
    json_file = open("templates_to_update.json", "w")
    json_file.write(json_string)
    json_file.close()


def is_template(item):
    return item['typeId'] == "11#template"


def template_contain_type_id(item):
    return "typeId" in item


def template_is_enable(item):
    return item['enable'] == 1
    # while 'LastEvaluatedKey' in response:
    #    response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
    #    data.extend(response['Items'])
#
# for item in data:
#    print(item)
