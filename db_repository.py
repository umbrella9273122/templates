import mysql.connector

mysql_connector = mysql.connector.connect(host="", database="", user="", password="", port=8889)
cursor = mysql_connector.cursor()


def fetch_all(sql):
    cursor.execute(sql)
    records = cursor.fetchall()
    return records


def fetch_one(query, id_param):
    cursor.execute(query, (id_param,))
    result = cursor.fetchone()
    return result[1] if result is not None else "undefined"
    # if result is not None:
    #     return result[1]
    # else:
    #     return "undefined"
